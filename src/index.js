import './bigOverLay.css';
import './PeopleFilter.css';

import React from 'react';
import ReactDOM from 'react-dom';
import BigOverLay from './BigOverLay.jsx';
import { Query, Builder, Preview, Buttons } from '../lib/ReactQueryBuilder';
import config from './config';

class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {showBasic: false, showHtml: false, showPeopleFilter: false};
  }

  _showBasic() {
    this.setState({showBasic: true});
  }

  _onHideBasic() {
    this.setState({showBasic: false});
  }

  _showHtml() {
    this.setState({showHtml: true});
  }

  _onHideHtml() {
    this.setState({showHtml: false});
  }

  _showPeopleFilter() {
    console.log('show people');
    this.setState({showPeopleFilter: true});
  }

  _onHidePeopleFilter() {
    this.setState({showPeopleFilter: false});
  }

  render() {
    return (
      <div>
        <ul className="list-style">
          <li><a href="#" className="btn btn-link" onClick={this._showBasic.bind(this)}>Open Basic Example</a></li>
          <li><a href="#" className="btn btn-link" onClick={this._showHtml.bind(this)}>Open Html Example</a></li>
          <li><a href="#" className="btn btn-link" onClick={this._showPeopleFilter.bind(this)}>Open People Filter Example</a></li>
        </ul>

        <BigOverLay show={this.state.showBasic} onHide={this._onHideBasic.bind(this)} header="Title">
          <div>
            Content
          </div>
        </BigOverLay>

        <BigOverLay show={this.state.showHtml} onHide={this._onHideHtml.bind(this)} header="Popover with custom HTML content">
          <div>
            <h3>Title</h3>
          </div>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sit amet massa massa. Aenean consectetur hendrerit ultrices. Nam semper, lacus quis viverra convallis, tortor nisl pretium lorem, at pulvinar dui erat in ante. Nam nec suscipit tellus, commodo vehicula ex. Proin hendrerit cursus fermentum. Mauris vel ligula dui. Cras eu erat vel neque accumsan consectetur. Etiam porta suscipit erat, ac vulputate nulla auctor ut. Praesent nec ex ac ex efficitur porttitor nec a eros.
          </p>
          <p>
            Etiam finibus dui vel nisi fringilla, sit amet malesuada arcu fermentum. Curabitur lobortis neque nunc, non venenatis ex sodales a. Mauris lobortis lorem ac neque luctus posuere. Donec a sodales ipsum, ac volutpat mauris. Duis accumsan lacinia enim congue hendrerit. Aliquam id convallis tellus. Sed lacinia sagittis risus, eget eleifend sem sagittis eget.
          </p>
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Column 1</th>
                <th>Column 2</th>
                <th>Column 3</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Value 1</td>
                <td>Value 2</td>
                <td>Value 3</td>
              </tr>
              <tr>
                <td>Value 1</td>
                <td>Value 2</td>
                <td>Value 3</td>
              </tr>
            </tbody>
          </table>
        </BigOverLay>

        <BigOverLay show={this.state.showPeopleFilter} onHide={this._onHidePeopleFilter.bind(this)} header="Advanced People Filter">
          <Query {...config}>
            {(props) => (
              <div>
                <div className="query-builder">
                  <Builder {...props} />
                </div>
                <Buttons {...props} onSave={this._onHidePeopleFilter.bind(this)}/>
              </div>
            )}
          </Query>
        </BigOverLay>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('container'));
