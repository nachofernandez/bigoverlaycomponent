import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';

class BigOverLay extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    var {header, children, ...others} = this.props;
    return (
      <Modal {...others} dialogClassName="big-overLay" >
        <Modal.Header closeButton>
          <Modal.Title>{header}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          {children}
        </Modal.Body>
      </Modal>
    );
  }
}

BigOverLay.propTypes = {
  children: React.PropTypes.node,
  header: React.PropTypes.node
};

module.exports = BigOverLay;
